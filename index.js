/*!
 * Api-Utils
 *
 * @author   Mobile Dev Team - IBM Argentina <santidesimone@ar.ibm.com>
 */

'use strict'

module.exports                      = apiUtils;

var crypto                          = require('crypto'),
    xml2js                          = require("xml2js");
const winston                       = require('winston');
winston.level                       = 'error';

/**
 * @class
*/
function apiUtils(options){
    this.xml2jsonBuilder            = new xml2js.Builder({
        headless                    : true,
        allowSurrogateChars         : true,
        renderOpts                  : {
            newline                 : ""
        }
    });

    this.json2xmlParser             = new xml2js.Parser({
        explicitArray               : false,
        ignoreAttrs                 : true,
        tagNameProcessors           : [xml2js.processors.stripPrefix]
    });

    this.defaultValue = options.defaultValue;
    this.validateChildsObject = options.validateChildsObject || false;
}

/**
 * Check if the given path exist inside an `object`, and return the value
 * @param {object} obj              - Object with the info to get
 * @param {array} path              - List of nested level / path to get the value
 */
apiUtils.prototype.getObjVal        = function(obj, path){
    var result                      = undefined;

    if(path.length>0 && obj){
        var actualVal               = obj[path[0]];
        if(actualVal){
            var path                = path.slice(1);
            result                  = this.getObjVal(actualVal, path);
        }
        else{
            result                  = (actualVal!=undefined)?actualVal:this.defaultValue;
        }
    }
    else{
        result                      = obj
    }

    return result;
}

/**
 * Encode a string into a base64, if an `Object` detected
 * @param {string} toEncode         - String to encode, if an `Object` type is detecte, apply a `JSON.stringify()`
 */
apiUtils.prototype.base64_encode    = function(toEncode) {
    var toEncode                    = (this.getType(toEncode) === 'object')?(JSON.stringify(toEncode)):toEncode,
        base64                      = new Buffer(toEncode).toString('base64');

    return base64;
}

/**
 * Decode a base64 string, if `isJson` param set to true, also will perfome a `JSON.parse()`
 * @param {string} encoded          - base64 String that should be decoded
 * @param {boolean} isJson          - set to true if the result should be converted to a object with �JSON.parse()�
 */
apiUtils.prototype.base64_decode    = function(encoded, isJson){
    var base64                      = new Buffer(encoded, 'base64').toString(),
        result                      = (isJson)?(JSON.parse(base64)):base64;

    return result;
}

/**
 * Cast a given `Date` into `String`, if no date is given, use today as default.
 * @param {date} date               - Date object or falsy to take today.
 * @param {string} separator        - used to fill / separate the parts of the date, Default is `""`.
 * @todo check if could be simplified with `moment`, could receive template �?, could even be deleted and translate that to the business login
 */
apiUtils.prototype.plainDate        = function(date, separator){
    var date                        = (date.constructor.name == "Date")? date : new Date(),
        separator                   = separator || "",
        year                        = date.getFullYear(),
        month                       = date.getMonth()+1,
        day                         = date.getDate(),
        result                      = "";

    month                           = (month<10)?"0"+month:month;
    day                             = (day<10)?"0"+day:day;
    result                          = ""+year+separator+month+separator+day;

    return result;
}

/**
 * Cast an Obj to JSON
 * @param {string} toCast   - Object to cast
 */
apiUtils.prototype.castToJson       = function(toCast){
    var result                      = {},
        toCast                      = toCast    || "";

    try{
        result                      = (toCast!="")?(this.getType(toCast) === "object")?toCast:JSON.parse(toCast):{};
    }
    catch(e){
        winston.log("error", "apiUtils -> castToJson => 'Fail in JSON cast' - name:: "+e.name+" code:: "+e.code+" msg:: "+e.message);
        result                      = {};
    }

    return result;
}

/**
 * Check if the passed valued has a valid constructor (From list `allowedTypes`)
 * @param {any} value           - Value to check
 * @param {array} allowedTypes  - List of valid constructors
 */
apiUtils.prototype.validateConstructor = function(value, allowedTypes) {
    var isTheConstructorAllowed     = false;

    if( allowedTypes && allowedTypes.constructor == Array ) {
        isTheConstructorAllowed     = (allowedTypes.indexOf(value.constructor) != -1);
    }

    return isTheConstructorAllowed;
}

/**
 * Validate if the input is `string` and not empty or at least has the minLength
 * @param {string} string       - Text to check
 * @param {number} [minLength]  - minLength, default in `1`
 */
apiUtils.prototype.validateString   = function(string, minLength) {
    var minLength                   = minLength || 1;
    var isValid                     = (this.getType(string) === "string") && ((string.trim()).length >= minLength);

    return isValid;
}

/**
 * Return a string with the data type, based in prototype.constructo`
 * @param {object} toCheck      - Value to check
 */
apiUtils.prototype.getType          = function(toCheck){
    var result                      = false;

    if([null, undefined].indexOf(toCheck)==-1){
        result                      = toCheck.constructor.toString().toLowerCase().match(/\s([a-z]*)/)[1];
    }
    else{
        result                      = ""+toCheck;
    }

    return result;
};

/**
 * Check if the value es an `Array`, if not it will include it into one
 * @return {array}
 */
apiUtils.prototype.ensureArray      = function(toEnsure){
    if(toEnsure.constructor!=Array){
        toEnsure        = [toEnsure]
    }
    return toEnsure;
};

/**
 * Return a base64 with a know length
 * @param {number} len      - length of the expected result
 * @return {string}
 */
apiUtils.prototype.randomValueBase64 = function(len){
    return crypto.randomBytes(Math.ceil(len * 3 / 4))
        .toString('base64')
        .slice(0, len)
        .replace(/\+/g, '-').replace(/\//g, '_'); // Convert to url safe
}

/**
 * Create a random integer between the `min` - `max` input
 * @param {number} min      -  min value.
 * @param {number} max      -  max value.
 * @return {number}
 */
apiUtils.prototype.getRandomInt         = function(min, max){
    return Math.floor(Math.random() * (max - min + 1)) + min;
}

/**
Permite traducir / interpretar un template para que tome informacion desde un origen y retorne el resultado ya relacionado
 * @param {from} any                    - Objecto raiz o proveedor de informacion
 * @param {dict} Object                 - Objecto template, con estructura JSON.
 * @return {result}
 */
apiUtils.prototype.interpreterCast      = function(from, dict, options){
    var result                          = (this.getType(dict)==="array")?[]:{},
        options                         = options || {},
        getValue                        = (origin, mapReference, opt) => {
            var response                = undefined;
            if((mapReference[0]===opt.literalChar)){
                mapReference            = mapReference.substr(1);
                response                = (opt.reverseMap)?this.getObjVal(origin, mapReference.split(opt.splitChar)):mapReference;
            }
            else{
                response                = (!opt.reverseMap)?this.getObjVal(origin, mapReference.split(opt.splitChar)):mapReference;
            }
            return response;
        },
        operationList                   = {
            "$forEach"                  : function(toIterate, resutlArr, actualDictionary, additionalParams, self){
                toIterate.forEach(function(itm, idx){
                    let regExpItm       = new RegExp("\\$"+additionalParams[0]+"\\"+options.splitChar+"|\\$"+additionalParams[0]+"$", "g"),
                        regExpIdx       = new RegExp("\\$"+additionalParams[1]+"\\"+options.splitChar+"|\\$"+additionalParams[1]+"$", "g"),
                        itmDict         = JSON.parse(JSON.stringify(actualDictionary[1])
                            .replace(regExpItm, itm+options.splitChar).replace(new RegExp("\\"+options.splitChar+"$", "g"),"")
                            .replace(regExpIdx, idx+options.splitChar).replace(new RegExp("\\"+options.splitChar+"$", "g"),""));
                    var temp            = (typeof itm == "string")?getValue(from, itmDict, options):self.interpreterCast(from, itmDict, options)
                    resutlArr.push(temp);
                }, self)
            }
        };

    options.reverseMap                  = options.reverseMap    || false;
    options.splitChar                   = options.splitChar     || ".";
    options.literalChar                 = options.literalChar   || "=";

    if(this.getType(dict)==="array"){
        dict                            = {
            "$temporalObject"           : dict
        }
    }

    for(let attr in dict) {
        if(typeof dict[attr] == "string") {
            result[attr]                = getValue(from, dict[attr], options);
        }
        else {
            if(this.getType(dict[attr])==="array"){
                let specialProcess      = ["$forEach"],
                    needSpecialProcess  = (this.getType(dict[attr][0]) === "string" && dict[attr][0].indexOf("$") === 0 && specialProcess.indexOf(dict[attr][0].split("[")[0]) === 0);

                result[attr]            = [];
                if(needSpecialProcess){
                    let splitedElements = dict[attr][0].replace(/]/g, "").split("["),
                        operation       = splitedElements[0],
                        dataPath        = splitedElements[1],
                        additionalParams= splitedElements[2].split(","),
                        toIterate       = getValue(from, dataPath, options) || [];

                    operationList[operation](toIterate, result[attr], dict[attr], additionalParams, this);
                }
                else{
                    dict[attr].forEach(function(itm){
                        var temp        = (typeof itm == "string")?getValue(from, itm, options):this.interpreterCast(from, itm, options)
                        result[attr].push(temp);
                    }, this)
                }

                if(attr === "$temporalObject"){
                    dict                = dict[attr];
                    result              = result[attr];
                }
            }
            else{
                if (this.getType(dict[attr])==="object") {
                    result[attr] = this.interpreterCast(from, dict[attr], options);
                    if(this.validateChildsObject)
                        result[attr] = (this.isEmptyObject(result[attr]))?this.defaultValue:result[attr];
                }
                else {
                    result[attr] =  getValue(from, dict[attr], options);
                }
            }
        }
    }
    return result;
}

apiUtils.prototype.isEmptyObject = function (obj){
    let self = this;
    for (let key in obj) {
        if (obj[key] != self.defaultValue)
            return false
    }
    return true;
}

apiUtils.prototype.tryCatchWrapper	 = function(toExecute, onError) {
    try{
        toExecute();
    }
    catch(e){
        onError(e);
    }
};

apiUtils.prototype.sortInObject = function (objectKey, key, order = 'asc') {
    return function (a, b) {
        if (typeof a === 'object' && typeof b === 'object') {
            a = a[objectKey];
            b = b[objectKey];
        }
        if (!a.hasOwnProperty(key) || !b.hasOwnProperty(key)) {
            return 0;
        }

        const varA = a[key];
        const varB = b[key];

        let comparison = 0;
        if (varA > varB) {
            comparison = 1;
        } else if (varA < varB) {
            comparison = -1;
        }
        return ((order == 'desc') ? (comparison * -1) : comparison);
    };
}

apiUtils.prototype.sortByDate = function (objectKey, key, order = 'asc') {
    return function (a, b) {
        if (typeof a === 'object' && typeof b === 'object') {
            a = a[objectKey];
            b = b[objectKey];
        }
        if (!a.hasOwnProperty(key) || !b.hasOwnProperty(key)) {
            return 0;
        }

        const varA = new Date(a[key]);
        const varB = new Date(b[key]);

        let comparison = 0;
        if (varA > varB) {
            comparison = 1;
        } else if (varA < varB) {
            comparison = -1;
        }
        return ((order == 'desc') ? (comparison * -1) : comparison);
    };
}

apiUtils.prototype.customSort = function (order, keys) {
    var o = order,
        nameKeys = keys;

    return function (a, b) {
        var order = o;

        for (var key in nameKeys) {
            a = a[nameKeys[key]];
            b = b[nameKeys[key]];
        }

        return order[a] - order[b];
    }
}

/**
 * Function that converts a XML chain to a JSON object
 * @param {xml} string - XML chain to be cast as JSON object
 * @return {jsonObject}
 */
apiUtils.prototype.xml2json        = function (xml) {
    var jsonObject                  = {};

    this.json2xmlParser.parseString(xml, function (err, result) {
        jsonObject                  = result;
    });

    return jsonObject;
}

/**
 * Funcion that build a string with XML format from a JSON string
 * @param {jsonToCast} object - JSON object that will be converted to XML
 * @return {xml}
 */
apiUtils.prototype.json2xml     = function (jsonToCast) {
    var xml                         = this.xml2jsonBuilder.buildObject(jsonToCast);
    xml                             = xml.replace(/&quot;/ig, "");
    return xml;
}

/**
* Force the transformation to an Array
* @param {*} toEnsure           - Anything to be enclosed into an Array
*/
apiUtils.prototype.ensureArray = function (toEnsure) {
    if(toEnsure.constructor!=Array){
        toEnsure        = [toEnsure]
    }
    return toEnsure;
};