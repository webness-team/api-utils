var apiUtils            = require('./../index'),
    apiUtilsInstance    = new apiUtils(),
    chai                = require ("chai"),
    assert              = chai.assert
//  expect              = chai.expect;


describe("#ensureArray()" , function(){

    it("should return array", function(done){
        assert.isArray(apiUtilsInstance.ensureArray({}), 'Operation unsuccessful');
        done();
    })

    it("should return array", function(done){
        assert.isArray(apiUtilsInstance.ensureArray("string"), 'Operation unsuccessful');
        done();
    })

    it("should return array", function(done){
        assert.isArray(apiUtilsInstance.ensureArray([]), 'Operation unsuccessful');
        done();
    })

    it("should return array", function(done){
        assert.isArray(apiUtilsInstance.ensureArray(true), 'Operation unsuccessful');
        done();
    })

    it("should return array", function(done){
        assert.isArray(apiUtilsInstance.ensureArray(false), 'Operation unsuccessful');
        done();
    })

// Not Passing:
// TODO: modify function so as to accept these values too

    // it("should return array", function(done){
    //     assert.isArray(apiUtilsInstance.ensureArray(undefined), 'Operation unsuccessful');
    //     done();
    // })

    // it("should return array", function(done){
    //     assert.isArray(apiUtilsInstance.ensureArray(null), 'Operation unsuccessful');
    //     done();
    // })

})


describe("#plainDate()" , function(){

    var date      = new Date(),
        separator = separator || "",
        year      = date.getFullYear(),
        month     = date.getMonth()+1,
        day       = date.getDate()
    month         = (month<10)?"0"+month:month
    day           = (day<10)?"0"+day:day
    var today     = ""+year+separator+month+separator+day;

    it("should be equal", function(done){
        assert.equal(apiUtilsInstance.plainDate(false), today, 'Operation unsuccessful');
        done();
    })

})



describe("#castToJson()" , function(){

    it("should return a defined value", function(done){
        assert.isDefined(apiUtilsInstance.castToJson({}), 'Operation unsuccessful');
        done();
    })

    it("output should be different from input", function(done){
        assert.notEqual(apiUtilsInstance.castToJson({}), {}, 'Operation unsuccessful');
        done();
    })

})


describe("#validateString()" , function(){

    it("should be valid", function(done){
        assert.isTrue(apiUtilsInstance.validateString("string"), 'Operation unsuccessful');
        done();
    })

    it("should be valid", function(done){
        assert.isTrue(apiUtilsInstance.validateString("string", 6), 'Operation unsuccessful');
        done();
    })

    it("should be invalid (too short string)", function(done){
        assert.isFalse(apiUtilsInstance.validateString("string", 20), 'Operation unsuccessful');
        done();
    })

    it("should be invalid (empty string)", function(done){
        assert.isFalse(apiUtilsInstance.validateString(""), 'Operation unsuccessful');
        done();
    })

    it("should be invalid", function(done){
        assert.isFalse(apiUtilsInstance.validateString({}), 'Operation unsuccessful');
        done();
    })

    it("should be invalid", function(done){
        assert.isFalse(apiUtilsInstance.validateString([]), 'Operation unsuccessful');
        done();
    })

    it("should be invalid", function(done){
        assert.isFalse(apiUtilsInstance.validateString(null), 'Operation unsuccessful');
        done();
    })

    it("should be invalid", function(done){
        assert.isFalse(apiUtilsInstance.validateString(undefined), 'Operation unsuccessful');
        done();
    })

})



describe("#getType()" , function(){

    it("should return 'array'", function(done){
        assert.equal(apiUtilsInstance.getType([]), 'array', 'Operation unsuccessful');
        done();
    })

    it("should return 'object'", function(done){
        assert.equal(apiUtilsInstance.getType({}), 'object', 'Operation unsuccessful');
        done();
    })

    it("should return 'boolean'", function(done){
        assert.equal(apiUtilsInstance.getType(false), 'boolean', 'Operation unsuccessful');
        done();
    })

    it("should return 'undefined'", function(done){
        assert.equal(apiUtilsInstance.getType(undefined), 'undefined', 'Operation unsuccessful');
        done();
    })

    it("should return 'null'", function(done){
        assert.equal(apiUtilsInstance.getType(null), 'null', 'Operation unsuccessful');
        done();
    })


})


describe("#randomValueBase64()" , function(){

    // it("should ...", function(done){
    //     assert.isArray(apiUtilsInstance.randomValueBase64(), 'Operation unsuccessful');
    //     done();
    // })

})


describe("#validateConstructor()" , function(){

    // it("should be valid", function(done){
    //     assert.isTrue(apiUtilsInstance.validateConstructor({}, ['Object']), 'Operation unsuccessful');
    //     done();
    // })
    //
    // it("should be valid", function(done){
    //     assert.isTrue(apiUtilsInstance.validateConstructor('string', ['String']), 'Operation unsuccessful');
    //     done();
    // })
    //
    //
    // it("should be valid", function(done){
    //     assert.isTrue(apiUtilsInstance.validateConstructor([], ['Array']), 'Operation unsuccessful');
    //     done();
    // })
    //
    //
    // it("should be invalid", function(done){
    //     assert.isFalse(apiUtilsInstance.validateConstructor({}, []), 'Operation unsuccessful');
    //     done();
    // })
    //
    // it("should be invalid", function(done){
    //     assert.isFalse(apiUtilsInstance.validateConstructor('string', []), 'Operation unsuccessful');
    //     done();
    // })
    //
    //
    // it("should be invalid", function(done){
    //     assert.isFalse(apiUtilsInstance.validateConstructor([], []), 'Operation unsuccessful');
    //     done();
    // })


})


describe("#getRandomInt()" , function(){

    // it("should ... ", function(done){
    //     assert.isArray(apiUtilsInstance.getRandomInt(), 'Operation unsuccessful');
    //     done();
    // })

})


describe("#getObjVal()" , function(){

    // it("should ... ", function(done){
    //     assert.isArray(apiUtilsInstance.getObjVal(), 'Operation unsuccessful');
    //     done();
    // })

})


describe("#base64_encode()" , function(){

    var base64String = new Buffer("string").toString('base64');
    it("should be equal ", function(done){
        assert.equal(apiUtilsInstance.base64_encode("string"), base64String, 'Operation unsuccessful');
        done();
    })


})


describe("#base64_decode()" , function(){

    // it("should ... ", function(done){
    //     assert.isArray(apiUtilsInstance.base64_decode(), 'Operation unsuccessful');
    //     done();
    // })

})
